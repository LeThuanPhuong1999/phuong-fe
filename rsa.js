const NodeRSA = require('node-rsa');


const genKey = () => {
    const key = new NodeRSA({ b: 1024 });
    const publicKey = key.exportKey('public')
    const privateKey = key.exportKey()
    return {
        publicKey,
        privateKey
    }
}

const encryptText = (text, publicKey) => {
    const key = new NodeRSA()
    key.importKey(publicKey, 'public')
    const encrypted = key.encrypt(text, 'base64');
    // console.log('encrypted: ', encrypted);
    // const decrypted = key.decrypt(encrypted, 'utf8');
    // console.log('decrypted: ', decrypted);
    return encrypted
}
const decryptText = (encryptText, privateKey) => {
    try {
        const key = new NodeRSA()
        key.importKey(privateKey, 'private')
        const decrypted = key.decrypt(encryptText, 'utf8');
        return decrypted
    } catch (error) {
        return null
    }
}

const publicKey = '-----BEGIN PUBLIC KEY-----\n' +
    'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCxCROfa9VRe7USANWqJImwbFsg\n' +
    '0tbrdEQK9sGiVlsiilztrcqxQYqqTLr2sUaI9NzENLr+VKeegIeZ/eF63QKtjlKR\n' +
    'zCQxCh9PqA0wN+MjfHs5K5CeXtP0NoCnqbq91HW9IjekaTnM8mLTt4EcmGRrOJ0u\n' +
    'c1Q1o7Ejg/PSyLcmpwIDAQAB\n' +
    '-----END PUBLIC KEY-----'
const privateKey = '-----BEGIN RSA PRIVATE KEY-----\n' +
    'MIICXAIBAAKBgQCxCROfa9VRe7USANWqJImwbFsg0tbrdEQK9sGiVlsiilztrcqx\n' +
    'QYqqTLr2sUaI9NzENLr+VKeegIeZ/eF63QKtjlKRzCQxCh9PqA0wN+MjfHs5K5Ce\n' +
    'XtP0NoCnqbq91HW9IjekaTnM8mLTt4EcmGRrOJ0uc1Q1o7Ejg/PSyLcmpwIDAQAB\n' +
    'AoGBAIYYcvWZb14mDvRRD4at4GFy4d/N9B8q1PNdDzSQpr1WAqfBsTtsgsUWE7QS\n' +
    'eXP5zVMNAertYn2rq4rYBDfVOlXJm2qBDhEqbr1CXEwdwINyszqWwDtyfD/pNY9z\n' +
    'tBmwgqeBixnD2Zlg6yQAE/uF3eYUdV+KXAF3seYulWolQOPRAkEA7KwM5BjEC7qr\n' +
    'i+wZnowXKD0rtP02FoVqsuxWBafHliSw2kJt6+MuuQfpRxI2TXOCLxyenpwdNaSY\n' +
    'YCAQHK0w/wJBAL9+PgFTm4c1pYsDReYj8OSAaBUjzZf9VSMIQCmSOZihPVq1+c1N\n' +
    'JLd2YJsvX0mmTcnCTbrC0Agrhw7pkNTA4lkCQGGjVSog4XgpSa5O9/7FsqQCIG7p\n' +
    'a4/8+JjaEV8B6cASYPeaIjkHn3XsQpv0cnyfY5zGBNZQd2hy//3gHGWIOccCQD03\n' +
    'h2z3JHZCESzym+rpvH3gCuYPLw+Svh/X6GMhyuXfTfTszfHy4nUAE/woVcb+skl4\n' +
    'o068LrMd7yw2QMWy7+kCQAVTsaky5myNNDxMOQyAQ7KH8WYFk/vEpda45lMF1irn\n' +
    '//G7vgtHwSP4giEazc3qB7XliAf+sCVFDvSj7VWMQSw=\n' +
    '-----END RSA PRIVATE KEY-----'

// console.log(genKey())
// console.log(encryptText('cuong', publicKey))
console.log(decryptText('TTLtcpdh3OyB2h+fBLswiZx/brqorQlbkLZXLcCqDjVGrZuGd96FYv258ow2wdj4txfqhzX4QLvrUhPuXNWztEcLOixell/6pqScgfgfosa5+FELo6Sy7IZ9Pe5nm9HdPZRwXAYqE3A17OFtFiTh8nFiwME2tEd7xFox+favGC8=', privateKey))