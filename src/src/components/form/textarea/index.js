import { memo } from 'preact/compat'
import {
  Textarea as VTextarea,
  Text
} from '@chakra-ui/core'



function Textarea({ placeholder = '', label = '', value = '', onValueChange }) {
  const handleOnChange = (e) => {
    onValueChange(e.target.value)
  }
  return (
    <>
      <Text mb="8px"> {label}</Text>
      <VTextarea
        value={value}
        placeholder={placeholder}
        size="sm"
        onChange={handleOnChange}
      />
    </>
  );
}

export default memo(Textarea)
