import { Box, Heading, Stack } from '@chakra-ui/core'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'
import AppItem from './AppItem'

export default function DropdownApp ({ currentApp }) {
  const { t } = useTranslation('global')
  return (
    <Box width='250px'>
      <Box py='sm' px='l'>
        <Heading fontSize='H300' lineHeight='H300'>
          {t('switchTo')}
        </Heading>
      </Box>
      <Stack px='sm' spacing='xs'>
        <AppItem
          icon={t('apps.monitoring.icon')}
          name={t('apps.monitoring.name')}
          href='https://global.ilotusland.com'
          target='_blank'
          color={t('apps.monitoring.color')}
        />
        <AppItem
          icon={t('apps.incident.icon')}
          name={t('apps.incident.name')}
          color={t('apps.incident.color')}
        />
        <AppItem
          icon={t('apps.device.icon')}
          name={t('apps.device.name')}
          color={t('apps.device.color')}
        />
        <AppItem
          icon={t('apps.maintenance.icon')}
          name={t('apps.maintenance.name')}
          color={t('apps.maintenance.color')}
        />{' '}
      </Stack>
    </Box>
  )
}

DropdownApp.propTypes = {
  currentApp: PropTypes.string
}
