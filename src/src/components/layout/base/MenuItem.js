import { Text, Icon, PseudoBox, Stack } from '@chakra-ui/core'
import { Link } from 'preact-router'
import PropTypes from 'prop-types'

export default function MenuItem ({
  icon,
  title,
  isActive,
  href,
  activePaths,
  ...props
}) {
  return (
    <PseudoBox
      as={Link}
      d='flex'
      direction='row'
      alignItems='center'
      p='sm'
      borderRadius='4px'
      color={isActive ? 'brand.primary' : '#42526e'}
      href={href}
      fontSize='bodyLarge'
      _hover={{
        color: 'brand.primary',
        cursor: 'pointer'
      }}
      _first={{ mt: 0 }}
      {...props}
    >
      <Stack justifyContent='center' alignItems='center' spacing='xxs'>
        <Icon size='24px' name={icon} mr='xs' />
        <Text fontSize='H200' lineHeight='H200' fontWeight='medium'>
          {title}
        </Text>
      </Stack>
    </PseudoBox>
  )
}

MenuItem.propTypes = {
  icon: PropTypes.string,
  title: PropTypes.string,
  isActive: PropTypes.bool,
  href: PropTypes.string,
  activePaths: PropTypes.array
}
