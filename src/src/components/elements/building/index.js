import { Stack, Image, Heading } from '@chakra-ui/core'

export default () => (
  <Stack spacing='xl' alignItems='center' mt='xl' p='xl'>
    <Image width='300px' src='/assets/img/building.png' />
    <Heading mt as='h1' fontSize='H400'>
      We'll building ...
    </Heading>
  </Stack>
)
