import { Stack, Text } from '@chakra-ui/core'
import PropTypes from 'prop-types'
import { normolizeUnit } from 'utils/unit'
import numeral from 'numeral'
import ElectricItem from '../electric-item'

export default function ElectricList ({
  items = [],
  primaryColor = 'gray.500'
}) {
  // console.log(items, 'itemsitemsitemsitems')
  return (
    <Stack spacing='md'>
      {items.map(item => (
        <Stack spacing='sm' key={item._id}>
          <Text color='gray.500'>{item.name}</Text>
          {item?.data.map(data => (
            <ElectricItem
              key={data._id}
              name={data.name}
              status={data.status}
              primary={`${numeral(data?.lastLogPVIn?.value).format(
                '0,0.00'
              )} (${normolizeUnit(data?.lastLogPVIn?.unit)})`}
              primaryColor={primaryColor}
              id={data._id}
              instantIndicator={data.lastLogs}
            />
          ))}
        </Stack>
      ))}
    </Stack>
  )
}

ElectricList.PropTypes = {
  unit: PropTypes.string,
  items: PropTypes.shape({
    _id: PropTypes.string,
    name: PropTypes.string,
    data: PropTypes.arrayOf(
      PropTypes.shape({
        _id: PropTypes.string,
        name: PropTypes.string,
        status: PropTypes.string,
        consume: PropTypes.number,
        lastLogPVIn: PropTypes.shape({
          _id: PropTypes.string,
          name: PropTypes.string,
          value: PropTypes.string,
          unit: PropTypes.string
        })
      })
    )
  })
}
