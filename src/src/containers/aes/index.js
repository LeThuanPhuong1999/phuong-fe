import { Box, Button } from '@chakra-ui/core'
import Heading from 'components/elements/heading'
import Textarea from 'components/form/textarea'
import { useState } from 'preact/hooks'


const Aes = () => {
    const [planText, setPlanText] = useState('')
    const [encodedText, setEncodedText] = useState('')
    return (
        <>
            <Box p='sm'>
                <Heading color='black' fontSize='H500' lineHeight='H500'>
                    AES
            </Heading>
            </Box>
            <Box px='sm'>
                <Textarea placeholder={"String to encode"} label={'String to encode'} value={planText} onValueChange={e => setPlanText(e)} />
                <Textarea placeholder={"String to decode"} label={'String to decode'} value={encodedText} onValueChange={e =>setEncodedText} />
                <Button onClick={generateKey} variantColor="green">Generate key</Button>
                <Button onClick={generateKey} variantColor="green">Generate key</Button>

            </Box>
        </>
    )
}
export default Aes