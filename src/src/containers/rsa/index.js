import { Box, Button } from '@chakra-ui/core'
import Heading from 'components/elements/heading'
import Textarea from 'components/form/textarea'
import { genKey, encryptText, decode } from 'api/phuongApi'
import { useState } from 'preact/hooks'

const Error = {
    INVALID_KEY: 'INVALID_KEY'
}




const Rsa = () => {
    const [privateKey, setPrivateKey] = useState('')
    const [privateKey4Decode, setPrivateKey4Decode] = useState('')
    const [publicKey, setPublicKey] = useState('')
    const [privateKeyGen, setPrivateKeyGen] = useState('')
    const [publicKeyGen, setPublicKeyGen] = useState('')
    const [planText, setPlanText] = useState('')
    const [encryptedText, setEncryptedText] = useState('')
    const [encryptedText4Decode, setEncryptedText4Decode] = useState('')
    const [output, setOutput] = useState('')

    const generateKey = async () => {
        let result = await genKey()
        setPublicKeyGen(result.data.publicKey)
        setPrivateKeyGen(result.data.privateKey)
    }
    const handleEncrypt = async () => {
        const result = await encryptText(planText, publicKey)
        if (result.error == true) alert(Error.INVALID_KEY)
        setEncryptedText(result.data)
    }
    const handleDecode = async () => {
        const result = await decode(encryptedText4Decode, privateKey4Decode)
        if (result.data == null) alert(Error.INVALID_KEY)
        setOutput(result.data)
    }
    const handleOnchangePlanText = (text) => {
        setPlanText(text)
    }
    const handleOnPublicKeyChange = (key) => {
        setPublicKey(key)
    }
    const handleChangePrivateKey = (key) => {
        setPrivateKey(key)
    }
    const handleEncryptdTextChange = (text) => {
        setEncryptedText4Decode(text)
    }
    const handleChangePrivateKey4Decode = key => {
        setPrivateKey4Decode(key)
    }
    return (
        <>
            <Box p='sm'>
                <Heading color='black' fontSize='H500' lineHeight='H500'>
                    RSA
                </Heading>
            </Box>
            <Box px='sm'>
                <Button onClick={generateKey} variantColor="green">Generate key</Button>
                <Textarea placeholder={"Public Key will appear here"} label={'Public Key'} value={publicKeyGen} />
                <Textarea placeholder={"Private Key will appear here"} label={'Private Key'} value={privateKeyGen} onValueChange={handleChangePrivateKey} />
                <Textarea placeholder={"Enter Plain Text to Encrypt"} label={'Enter Plain Text to Encrypt'} value={planText} onValueChange={handleOnchangePlanText} />
                <Textarea placeholder={"Enter Public key"} label={'Enter Public key'} value={publicKey} onValueChange={handleOnPublicKeyChange} />
                <Textarea placeholder={"Encrypted Output"} label={'Encrypted Output (Base64):'} value={encryptedText} />
                <Button onClick={handleEncrypt} variantColor="green">Encrypt</Button>

                <Textarea placeholder={"Enter Encrypted Text to Decrypt (Base64)"} label={'Enter Encrypted Text to Decrypt'} onValueChange={handleEncryptdTextChange} value={encryptedText4Decode} />
                <Textarea placeholder={"Enter Private key"} label={'Enter Private key'} onValueChange={handleChangePrivateKey4Decode} value={privateKey4Decode} />
                <Textarea placeholder={"Decrypted Output"} label={'Decrypted Output:'} value={output} />
                <Button variantColor="green" onClick={handleDecode}>Decrypt</Button>

            </Box>
        </>
    )
}
export default Rsa